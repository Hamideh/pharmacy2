package ir.fardup.demo.factor.Controller;

import lombok.Data;

import java.util.Collection;

@Data
public class FactorModel {
    private Integer id;
    private Integer totalPrice;
   // private Integer price;
    private Integer count;
    private Collection<FactorDetailModel> factorDetailModels;


}
