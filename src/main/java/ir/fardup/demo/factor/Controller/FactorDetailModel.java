package ir.fardup.demo.factor.Controller;

import lombok.Data;

@Data
public class FactorDetailModel {
    private Integer id;
    private Integer count;
    private Integer totalPriceDetail;
    private Integer price;
    private String drugName;

    private Integer drugId;
    private Integer factorId;
}
