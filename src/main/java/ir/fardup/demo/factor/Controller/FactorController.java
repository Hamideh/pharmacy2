package ir.fardup.demo.factor.Controller;
import ir.fardup.demo.factor.Service.FactorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/factor")
public class FactorController {

    private FactorService factorService;

    @Autowired
    public FactorController(FactorService factorService) {
        this.factorService = factorService;
    }

    @GetMapping(value = {"/", ""})
    public List<FactorModel> index() { return factorService.index();
    }

    @PostMapping({"/", ""})
    public FactorModel create(@RequestBody FactorModel factormodel) {
        return factorService.create(factormodel);
    }

    @PutMapping({"/", ""})
    public FactorModel update(@RequestBody FactorModel factormodel) {
        return factorService.update(factormodel);
    }

    @DeleteMapping({"/{id}"})
    public Boolean delete(@PathVariable Integer id) {
        return factorService.delete(id);
    }
}

