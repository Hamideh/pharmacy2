package ir.fardup.demo.factor.Orm;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FactorRepository extends JpaRepository<Factor,Integer> {
}
