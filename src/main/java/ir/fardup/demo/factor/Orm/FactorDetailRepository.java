package ir.fardup.demo.factor.Orm;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FactorDetailRepository extends JpaRepository<FactorDetail,Integer> {
}
