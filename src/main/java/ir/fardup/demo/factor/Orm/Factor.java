package ir.fardup.demo.factor.Orm;

import ir.fardup.demo.user.Orm.User;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "factor")
@Data
public class Factor {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "count")
    private Integer count;
    //@Column(name = "price")
   // Integer price;
    @Column(name = "total_price")
    private Integer totalPrice;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    User user;

    @OneToMany(fetch = FetchType.LAZY ,mappedBy = "factor")
    Collection<FactorDetail> factorDetail;

}
