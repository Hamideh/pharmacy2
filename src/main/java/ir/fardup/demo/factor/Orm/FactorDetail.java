package ir.fardup.demo.factor.Orm;

import ir.fardup.demo.drug.Orm.Drug;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "factor_detail")
@Data
public class FactorDetail {
   @Id
   @Column(name = "id")
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Integer id;
   @Column(name = "drug_name")
   private String drugName;
   @Column(name = "price")
   private Integer price;
   @Column(name = "total_price")
   private Integer totalPrice;
   @Column(name = "count")
   private Integer count;

   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "drug_id" ,referencedColumnName = "id")
   Drug drug;

   @ManyToOne(fetch = FetchType.LAZY )
   @JoinColumn(name = "factor_id",referencedColumnName = "id")
   Factor factor;

}
