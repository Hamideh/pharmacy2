package ir.fardup.demo.factor.Service;

import ir.fardup.demo.drug.Controller.DrugModel;
import ir.fardup.demo.factor.Controller.FactorModel;
import ir.fardup.demo.factor.Orm.Factor;

import java.util.List;

public interface FactorService {
    FactorModel create(FactorModel factorModel);

    List<FactorModel> index();

    FactorModel update(FactorModel factorModel);

    Boolean delete(Integer id);
}

