package ir.fardup.demo.factor.Service;

import ir.fardup.demo.drug.Controller.DrugModel;
import ir.fardup.demo.drug.Orm.Drug;
import ir.fardup.demo.drug.Orm.DrugRepository;
import ir.fardup.demo.drug.Service.DrugService;
import ir.fardup.demo.factor.Controller.FactorDetailModel;
import ir.fardup.demo.factor.Controller.FactorModel;
import ir.fardup.demo.factor.Orm.Factor;
import ir.fardup.demo.factor.Orm.FactorDetail;
import ir.fardup.demo.factor.Orm.FactorDetailRepository;
import ir.fardup.demo.factor.Orm.FactorRepository;
import org.hibernate.query.criteria.internal.expression.function.AggregationFunction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Qualifier(value = "FactorServiceImp")
@Transactional(rollbackFor = Exception.class)
public class FactorServiceImp implements FactorService {

    private FactorRepository factorRepository;
    private DrugService drugService;
    private DrugRepository drugRepository;
    private FactorDetailRepository factorDetailRepository;
    @Autowired

    public FactorServiceImp(FactorRepository factorRepository, DrugService drugService, DrugRepository drugRepository, FactorDetailRepository factorDetailRepository) {
        this.factorRepository = factorRepository;
        this.drugService = drugService;
        this.drugRepository = drugRepository;
        this.factorDetailRepository = factorDetailRepository;
    }

    @Override
    public List<FactorModel> index() {
        return factorRepository.findAll().stream().map(value -> this.convertToModel(value)).collect(Collectors.toList());
    }

    @Override
    public FactorModel create(FactorModel factorModel) {
        Factor factor = factorRepository.save(this.convertToEntity(factorModel));

        List<FactorDetail> factorDetailList=factorDetailRepository.saveAll(factorModel.getFactorDetailModels().stream().map( t->{
            t.setFactorId(factor.getId());
            t.setTotalPriceDetail(t.getPrice()*t.getCount());
            return convertToEntity(t);
        }).collect(Collectors.toList()));

        Integer totalFactorPrice=0;
        Integer factorCount=0;
        for (FactorDetail factorDetail:factorDetailList) {
            totalFactorPrice+=factorDetail.getTotalPrice();
            factorCount+=factorDetail.getCount();
        }

        factor.setTotalPrice(totalFactorPrice);
        factor.setCount(factorCount);
        factor.setFactorDetail(factorDetailList);

        Factor factor2 = factorRepository.save(factor);

        return convertToModel(factor2);
    }

    @Override
    public FactorModel update(FactorModel factorModel) {
        factorRepository.save(this.convertToEntity(factorModel));
        return factorModel;
    }

    @Override
    public Boolean delete(Integer id) {
        try {
            factorRepository.deleteById(id);
        } catch (Exception e) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    private FactorModel convertToModel(Factor factor) {
        FactorModel factorModel = new FactorModel();
        factorModel.setId(factor.getId());
        //factorModel.setPrice(factor.getPrice());
        factorModel.setTotalPrice(factor.getTotalPrice());
        factorModel.setCount(factor.getCount());

        if(factor.getFactorDetail()!=null){
            factorModel.setFactorDetailModels(factor.getFactorDetail().stream()
                    .map(this::convertToModel).collect(Collectors.toList()));
        }
        return factorModel;
    }

    private Factor convertToEntity(FactorModel factorModel) {
        Factor factor = new Factor();
        factor.setId(factorModel.getId());
        //factor.setPrice(factorModel.getPrice());
        factor.setTotalPrice(factorModel.getTotalPrice());
        factor.setCount(factorModel.getCount());
        return factor;
    }

    private FactorDetailModel convertToModel(FactorDetail factorDetail){
        FactorDetailModel factorDetailModel=new FactorDetailModel();
        factorDetailModel.setId(factorDetail.getId());
        factorDetailModel.setDrugId(factorDetail.getDrug().getId());
        factorDetailModel.setDrugName(factorDetail.getDrugName());
        factorDetailModel.setFactorId(factorDetail.getFactor().getId());
        factorDetailModel.setCount(factorDetail.getCount());
        factorDetailModel.setPrice(factorDetail.getPrice());
        factorDetailModel.setTotalPriceDetail(factorDetail.getTotalPrice());
        return factorDetailModel;

    }

    private FactorDetail convertToEntity(FactorDetailModel factorDetailModel) {
        FactorDetail factorDetail = new FactorDetail();
        factorDetail.setId(factorDetailModel.getId());
        Drug drug=drugRepository.findById(factorDetailModel.getDrugId()).get();
        factorDetail.setDrugName(drug.getName());
        factorDetail.setDrug(drug);
        factorDetail.setCount(factorDetailModel.getCount());
        factorDetail.setPrice(factorDetailModel.getPrice());
        factorDetail.setTotalPrice(factorDetailModel.getTotalPriceDetail());
        Factor factor=new Factor();
        factor.setId(factorDetailModel.getFactorId());
        factorDetail.setFactor(factor);
        return factorDetail;
    }
}