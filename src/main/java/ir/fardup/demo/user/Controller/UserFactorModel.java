package ir.fardup.demo.user.Controller;

import lombok.Data;

@Data
public class UserFactorModel {
    Integer id;
    String name;
}
