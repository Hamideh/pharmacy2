package ir.fardup.demo.user.Controller;

import javafx.print.Collation;
import lombok.Data;

import java.util.List;

@Data
public class UserModel {
    Integer id;
    String name;
private List<UserFactorModel> factormodels;
}
