package ir.fardup.demo.user.Controller;

import ir.fardup.demo.factor.Controller.FactorModel;
import ir.fardup.demo.user.Orm.User;
import ir.fardup.demo.user.Service.UserServise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/user")
public class UserController {
private UserServise userServise;
    @GetMapping(value = {"/", ""})
    public List<UserModel> index() { return userServise.index();
    }

    @PostMapping({"/", ""})
    public UserModel create(@RequestBody UserModel userModel) {
        return userServise.create(userModel);
    }

    @PutMapping({"/", ""})
    public UserModel update(@RequestBody UserModel userModel) {
        return userServise.update(userModel);
    }

    @DeleteMapping({"/{id}"})
    public Boolean delete(@PathVariable Integer id) {
        return userServise.delete(id);
    }
}











