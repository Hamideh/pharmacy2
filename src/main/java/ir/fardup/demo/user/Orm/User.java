package ir.fardup.demo.user.Orm;

import ir.fardup.demo.factor.Orm.Factor;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "user")
@Data
public class User  {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Column(name = "name")
    String name;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "user")
    Collection<Factor> factors;
}
