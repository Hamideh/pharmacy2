package ir.fardup.demo.user.Service;

import ir.fardup.demo.category.Orm.CategoryRepository;
import ir.fardup.demo.category.Service.CategoryService;
import ir.fardup.demo.factor.Orm.Factor;
import ir.fardup.demo.factor.Orm.FactorRepository;
import ir.fardup.demo.user.Controller.UserFactorModel;
import ir.fardup.demo.user.Controller.UserModel;
import ir.fardup.demo.user.Orm.User;
import ir.fardup.demo.user.Orm.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Qualifier(value = "UserServiceImp")
@Transactional(rollbackFor = Exception.class)
public class UserServiceImp implements UserServise {
    private UserRepository userRepository;

    private FactorRepository factorRepository;

    @Autowired

    public UserServiceImp(UserRepository userRepository, FactorRepository factorRepository) {
        this.userRepository = userRepository;
        this.factorRepository = factorRepository;
    }


    @Override
    public List<UserModel> index() {
        return userRepository.findAll().stream().map(value -> this.convertToModel(value)).collect(Collectors.toList());
    }

    @Override
    public UserModel create(UserModel userModel) {
        User user = userRepository.save(this.convertToEntity(userModel));
        userModel.setId(userModel.getId());
        return userModel;
    }

    @Override
    public UserModel update(UserModel userModel) {
        userRepository.save(this.convertToEntity(userModel));
        return userModel;
    }

    @Override
    public Boolean delete(Integer id) {
        try {
            userRepository.deleteById(id);
        } catch (Exception e) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    private UserModel convertToModel(User user) {
        UserModel userModel = new UserModel();
        userModel.setId(user.getId());
        userModel.setName(user.getName());
        if (user.getFactors() != null) {
            user.getFactors().stream().map(this::convertToUserFactorModel);
        }

        return userModel;
    }

    private User convertToEntity(UserModel userModel) {
        User user = new User();
        user.setId(userModel.getId());
        user.setName(userModel.getName());
        return user;
    }
    private UserFactorModel  convertToUserFactorModel(Factor factor){
        UserFactorModel userFactorModel=new UserFactorModel();
        userFactorModel.setId(factor.getId());
        return userFactorModel;
    }
}
