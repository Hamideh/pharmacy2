package ir.fardup.demo.user.Service;

import ir.fardup.demo.user.Controller.UserModel;

import java.util.List;

public interface UserServise {
    UserModel create(UserModel userModel);

    List<UserModel> index();

    UserModel update(UserModel userModel);

    Boolean delete(Integer id);
    }

