package ir.fardup.demo.category.Orm;


import ir.fardup.demo.drug.Orm.Drug;
import lombok.Data;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "category")
@Data
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;

    @OneToMany(fetch =FetchType.LAZY, mappedBy = "category")
    Collection<Drug> drugs;
}

