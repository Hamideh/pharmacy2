package ir.fardup.demo.category.Service;

import ir.fardup.demo.category.Controller.CategoryModel;

import java.util.List;

public interface CategoryService {
    List<CategoryModel> index();
    CategoryModel create(CategoryModel categoryModel);

    CategoryModel update(CategoryModel categoryModel);

    Boolean delete(Integer id);
}

