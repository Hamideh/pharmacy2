package ir.fardup.demo.category.Service;

import ir.fardup.demo.category.Controller.CategoryDrugModel;
import ir.fardup.demo.category.Controller.CategoryModel;
import ir.fardup.demo.category.Orm.Category;
import ir.fardup.demo.category.Orm.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Qualifier("CategoryServiceImp")
@Transactional(rollbackFor = Exception.class)
public class CategoryServiceImp implements CategoryService{
private CategoryRepository categoryRepository;
    @Autowired

    public CategoryServiceImp(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }


    @Override
    public List<CategoryModel> index() {
        return categoryRepository.findAll().stream().map(value -> this.convertToModel(value)).collect(Collectors.toList());
    }

    @Override
    public CategoryModel create(CategoryModel categoryModel) {
        Category category=categoryRepository.save(this.convertToEntity(categoryModel));
        categoryModel.setId(category.getId());
        return categoryModel;
    }

    @Override
    public CategoryModel update(CategoryModel categoryModel) {
        categoryRepository.save(this.convertToEntity(categoryModel));
        return categoryModel;
    }

    @Override
    public Boolean delete(Integer id) {
        try {
            categoryRepository.deleteById(id);
        } catch (Exception e) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    private CategoryModel convertToModel(Category category){
    CategoryModel categoryModel=new CategoryModel();
    categoryModel.setId(category.getId());
    categoryModel.setName(category.getName());
    return categoryModel;
    }
    private Category convertToEntity(CategoryModel categoryModel){
        Category category=new Category();
        category.setId(categoryModel.getId());
        category.setName(categoryModel.getName());
        return category;
    }
   // private CategoryDrugModel convertToModel(Category)





}