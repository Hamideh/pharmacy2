package ir.fardup.demo.category.Controller;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Data
public class CategoryModel {
private Integer id;
private String name;
private List<CategoryDrugModel> drugModels;
}
