package ir.fardup.demo.category.Controller;

import ir.fardup.demo.category.Service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/category")
public class CategoryController {

    private CategoryService categoryService;
    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping(value = {"/", ""})
    public List<CategoryModel> index() {
        return categoryService.index();
    }

    @PostMapping({"/", ""})
     public CategoryModel create(@RequestBody CategoryModel categoryModel)
    {return categoryService.create(categoryModel);}

    @PutMapping(value = {"/",""})
   public CategoryModel update(@RequestBody CategoryModel categoryModel)
{return categoryService.update(categoryModel);}

@DeleteMapping(value = {"id"})
public boolean delete(@PathVariable Integer id)
{return categoryService.delete(id); }
}