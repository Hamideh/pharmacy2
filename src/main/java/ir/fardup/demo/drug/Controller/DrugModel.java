package ir.fardup.demo.drug.Controller;

import lombok.Data;

@Data
public class DrugModel {
    private Integer id;
    private Integer productionYear;
   private String name;
   private Float tax;
   private Float price;

   private Integer expirationYear;

   private Integer category_id;

   private String category_name;

}
