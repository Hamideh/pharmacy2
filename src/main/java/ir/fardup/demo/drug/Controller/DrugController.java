package ir.fardup.demo.drug.Controller;

import ir.fardup.demo.drug.Service.DrugService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "drug")
public class DrugController {
    private DrugService drugService;
    @Autowired

    public DrugController(DrugService drugService) {
        this.drugService = drugService;
    }

    @GetMapping(value = {"/", ""})
    public List<DrugModel> index() {
        return drugService.index();
    }

    @PostMapping({"/", ""})
    public DrugModel create(@RequestBody DrugModel DrugModel) {
        return drugService.create(DrugModel);
    }

    @PutMapping({"/", ""})
    public DrugModel update(@RequestBody DrugModel DrugModel) {
        return drugService.update(DrugModel);
    }

    @DeleteMapping({"/{id}"})
    public Boolean delete(@PathVariable Integer id) {
        return drugService.delete(id);
    }





}
