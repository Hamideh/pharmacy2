package ir.fardup.demo.drug.Orm;

import ir.fardup.demo.category.Orm.Category;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "drug")
@Data
public class Drug {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;
    @Column(name = "name")
    String name;
    @Column(name = "production_ِyear")
    Integer productionYear;
    @Column(name = "expiration_year")
    Integer expirationYear;
    @Column(name = "price")
    Float price;
    @Column(name = "tax")
    Float tax;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id",referencedColumnName = "id")
    Category category;


}
