package ir.fardup.demo.drug.Service;

import ir.fardup.demo.category.Orm.CategoryRepository;
import ir.fardup.demo.category.Service.CategoryService;
import ir.fardup.demo.drug.Controller.DrugModel;
import ir.fardup.demo.drug.Orm.Drug;
import ir.fardup.demo.drug.Orm.DrugRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Qualifier("DrugServiceImp")
@Transactional(rollbackFor = Exception.class)
public class DrugServiceImp implements DrugService {
    private DrugRepository drugRepository;

    private CategoryService categoryService;

    private CategoryRepository categoryRepository;

    @Autowired
    public DrugServiceImp(DrugRepository drugRepository, CategoryService categoryService, CategoryRepository categoryRepository) {
        this.drugRepository = drugRepository;
        this.categoryService = categoryService;
        this.categoryRepository = categoryRepository;
    }


    @Override
    public List<DrugModel> index() {
        return drugRepository.findAll().stream().map(value -> this.convertToModel(value)).collect(Collectors.toList());
    }

    @Override
    public DrugModel create(DrugModel drugModel) {
        Drug drug = drugRepository.save(this.convertToEntity(drugModel));
        drugModel.setId(drugModel.getId());
        return drugModel;
    }

    @Override
    public DrugModel update(DrugModel drugModel) {
        drugRepository.save(this.convertToEntity(drugModel));
        return drugModel;
    }

    @Override
    public Boolean delete(Integer id) {
        try {
            drugRepository.deleteById(id);
        } catch (Exception e) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    private DrugModel convertToModel(Drug drug) {
        DrugModel drugModel = new DrugModel();
        drugModel.setId(drug.getId());
        drugModel.setName(drug.getName());
        drugModel.setExpirationYear(drug.getExpirationYear());
        drugModel.setProductionYear(drug.getProductionYear());
        drugModel.setPrice(drug.getPrice());
        drugModel.setTax(drug.getTax());

        Optional.ofNullable(drug.getCategory()).ifPresent(category -> {
            drugModel.setCategory_name(category.getName());
            drugModel.setCategory_id(category.getId());});
        return drugModel;
    }
        private Drug convertToEntity(DrugModel drugModel){
        Drug drug=new Drug();
        drug.setId(drugModel.getId());
        drug.setName(drugModel.getName());
        drug.setPrice(drugModel.getPrice());
        drug.setTax(drugModel.getTax());
        drug.setExpirationYear(drugModel.getExpirationYear());
        drug.setProductionYear(drugModel.getProductionYear());

        if (drugModel.getCategory_id() != null){
            categoryRepository.findById(drugModel.getCategory_id()).ifPresent(drug::setCategory);
        }
        return drug;
        }
}
