package ir.fardup.demo.drug.Service;

import ir.fardup.demo.drug.Controller.DrugModel;

import java.util.List;

public interface DrugService {
    DrugModel create(DrugModel drugModel);

    List<DrugModel> index();

    DrugModel update(DrugModel drugModel);

    Boolean delete(Integer id);
}
